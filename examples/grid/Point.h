#ifndef POINT_H
#define POINT_H

class Point {
public:
    double x;
    double y;
    double z;
    double vx;
    double vy;
    double vz;

    Point(double x, double y, double z, double vx, double vy, double vz):
        x(x),y(y),z(z),vx(vx),vy(vy),vz(vz) {};

    void UpdatePosition(double dt) {
        x += dt*vx;
        y += dt*vy;
        z += dt*vz;
    }

    void print() {
        std::cout << "(" << x << ", " << y << ", " << z << ", "
                  << vx << ", " << vy << ", " << vz << ")" << std::endl;
    }
};
    
#endif //POINT_H
