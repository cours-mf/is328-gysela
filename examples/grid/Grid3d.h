#ifndef GRID3D_H
#define GRID3D_H

#include <vector>

#include "Point.h"

class Grid3d {
public:
	std::vector<int> leading_dims;
	std::vector<double> mini;
	std::vector<double> maxi;
	std::vector<double> spacing;
    int N_cells;
    Grid3d(std::vector<int> ld, std::vector<double> min, std::vector<double> max) {
        leading_dims = ld;
        mini = min;
        maxi = max;
        N_cells = (ld[0]-1)*(ld[1]-1)*(ld[2]-1);
        spacing.emplace_back((maxi[0]-mini[0])/(ld[0]-1));
        spacing.emplace_back((maxi[1]-mini[1])/(ld[1]-1));
        spacing.emplace_back((maxi[2]-mini[2])/(ld[2]-1));
    };

    std::vector<int> count(std::vector<Point> &points) {
        std::vector<int> counting_field(N_cells, 0);
        for (Point &p : points) {
            int ix = std::floor((p.x - mini[0])/spacing[0]);
            int iy = std::floor((p.y - mini[1])/spacing[1]);
            int iz = std::floor((p.z - mini[2])/spacing[2]);
            int id = ix + (leading_dims[0]-1)*(iy + (leading_dims[1]-1)*iz);
            if (0 <= id && N_cells > id)
                counting_field[id]++;
        }

        return counting_field;
    }
};
    
#endif //GRID3D_H
