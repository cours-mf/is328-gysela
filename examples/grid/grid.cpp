#include <cstdlib>
#include <iostream>

#include "CatalystAdaptor.h"
#include "Point.h"
#include "Grid3d.h"

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        std::cerr << "Usage: ./simple <path_to_pipeline_script> [--live]"
                  << std::endl;
        return EXIT_FAILURE;
    }

    // Get pipeline python script
    const char* pipeline_script = argv[1];

    // Get libcatalyst-paraview.so directory
    const char* paraview_lib_catalyst_dir = std::getenv("PARAVIEW_LIB_CATALYST_DIR");
    if (!paraview_lib_catalyst_dir) {
        std::cout << "Please set PARAVIEW_LIB_CATALYST_DIR (see install instructions)" << std::endl;
        return EXIT_FAILURE;
    }

    // Get option
    std::string option;
    if (argc == 3)
        option = std::string(argv[2]);

    // Create catalyst adaptor
    std::cout << "Simple:" << std::endl
              << "=======" << std::endl;
    CatalystAdaptor catalyst(paraview_lib_catalyst_dir, pipeline_script, option);

    // Setup a grid
    int grid_ld = 15; // number of mesh node (= number of cells+1)
    std::vector<double> mini{-1, -5, -4};
    std::vector<double> maxi{5, 8, 7};
    std::vector<int> dims{grid_ld, grid_ld, grid_ld};
    Grid3d grid(dims, mini, maxi);
    
    // Init some points
    int N_points = 1000;
    std::vector<Point> points;
    points.reserve(N_points);
    for (int i = 0; i < N_points; i++) {
        points.emplace_back(
            ((double)(std::rand()%((int)(100*(maxi[0]-mini[0])))))/100 + mini[0],
            ((double)(std::rand()%((int)(100*(maxi[1]-mini[1])))))/100 + mini[1],
            ((double)(std::rand()%((int)(100*(maxi[2]-mini[2])))))/100 + mini[2],
            ((double)(std::rand()%(100*5)))/100 - 2.5,
            ((double)(std::rand()%(100*5)))/100 - 2.5,
            ((double)(std::rand()%(100*5)))/100 - 2.5
            );
        //points[i].print();
    }
    
    // Perform simulation
    int N_iter = 20;
    double dt = 0.01;
    for (int iter = 0; iter < N_iter; iter++) {
        // Count the number of points in each grid cell
        std::vector<int> count = grid.count(points);
        
        // Render
        catalyst.execute(iter, dt*iter, grid, count);

        // Update points
        for (Point &point : points) {
            point.UpdatePosition(dt);
        }
    }

    return EXIT_SUCCESS;
}
