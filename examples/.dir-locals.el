;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((c++-mode . ((fill-column . 90)
              (tab-width . 4)
              (indent-tabs-mode . nil))))
