#include <iostream>

#include "CatalystAdaptor.h"

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        std::cerr << "Usage: ./simple <path_to_pipeline_script> [--live]"
                  << std::endl;
        return EXIT_FAILURE;
    }

    // Get pipeline python script
    const char* pipeline_script = argv[1];

    // Get libcatalyst-paraview.so directory
    const char* paraview_lib_catalyst_dir = std::getenv("PARAVIEW_LIB_CATALYST_DIR");
    if (!paraview_lib_catalyst_dir) {
        std::cout << "Please set PARAVIEW_LIB_CATALYST_DIR (see install instructions)" << std::endl;
        return EXIT_FAILURE;
    }

    // Get option
    std::string option;
    if (argc == 3)
        option = std::string(argv[2]);

    // Create catalyst adaptor
    std::cout << "Simple:" << std::endl
              << "=======" << std::endl;
    CatalystAdaptor catalyst(paraview_lib_catalyst_dir, pipeline_script, option);

    // Perform simulation
    catalyst.execute(0, 0.0);
    catalyst.execute(1, 0.2);
    catalyst.execute(2, 0.5);

    return EXIT_SUCCESS;
}
