#ifndef CATALYSTS_ADAPTOR_H
#define CATALYSTS_ADAPTOR_H

#include <catalyst.hpp>

namespace Conduit = conduit_cpp;

class CatalystAdaptor {
public:
    CatalystAdaptor(std::string catalyst_implementation_dir,
                    std::string script_name, std::string option);
    ~CatalystAdaptor();
    void execute(int timestep, double time);
};


CatalystAdaptor::CatalystAdaptor(std::string catalyst_implementation_dir,
                                 std::string script_name, std::string option) {
    Conduit::Node node;

    // TODO: Remplir le noeud avant de l'envoyer

    std::cout << "-- Init node : sending conduit..." << std::endl;
    catalyst_status err = catalyst_initialize(conduit_cpp::c_node(&node));
    if (err != catalyst_status_ok) {
        std::cerr << "Failed to initialize Catalyst: " << err << std::endl;
    }
    std::cout << "-- Init node : sent" << std::endl;
};

CatalystAdaptor::~CatalystAdaptor() {
    Conduit::Node node;

    // TODO: Remplir le noeud avant de l'envoyer

    std::cout << "-- Fini node : sending conduit..." << std::endl;
    catalyst_status err = catalyst_finalize(Conduit::c_node(&node));
    if (err != catalyst_status_ok) {
        std::cerr << "Failed to finalize Catalyst: " << err << std::endl;
    }
    std::cout << "-- Fini node : sent" << std::endl;
};

void
CatalystAdaptor::execute(int timestep, double time) {
    Conduit::Node node;

    // TODO: Remplir le noeud avant de l'envoyer

    std::cout << "-- Exec node : sending conduit..." << std::endl;
    catalyst_status err = catalyst_execute(conduit_cpp::c_node(&node));
    if (err != catalyst_status_ok) {
        std::cerr << "Failed to execute Catalyst: " << err << std::endl;
    }
    std::cout << "-- Exec node : sent" << std::endl;
};

#endif // CATALYST_ADAPTORS_H
