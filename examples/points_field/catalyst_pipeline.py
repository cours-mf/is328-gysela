# script-version: 2.0
from paraview.simple import *
from paraview import catalyst
import time

# The whole script is interpreted at initialize
print("-- Init Catalyst pipeline with options {} :".format(catalyst.get_args()))

# Set Catalyst options
options = catalyst.Options()
if "--live" in catalyst.get_args():
  options.EnableCatalystLive = 1

# C++ data producer
producer = TrivialProducer(registrationName="my_points")

# Define the function that will be called at each execute
def catalyst_execute(state):
    global producer
    producer.UpdatePipeline()
    info = producer.GetDataInformation()
    print("  * Time : (timestep={}, time={})".format(state.timestep, state.time))
    print("    - bounds:", info.GetBounds())
    print("    - points:", info.GetNumberOfPoints())
    print("    - cells:", info.GetNumberOfCells())
    print("    - velocity range:", producer.PointData["my_velocity"].GetRange(0))
    print(producer.PointData.keys())
