#include <iostream>

#include "CatalystAdaptor.h"
#include "Point.h"

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        std::cerr << "Usage: ./simple <path_to_pipeline_script> [--live]"
                  << std::endl;
        return EXIT_FAILURE;
    }

    // Get pipeline python script
    const char* pipeline_script = argv[1];

    // Get libcatalyst-paraview.so directory
    const char* paraview_lib_catalyst_dir = std::getenv("PARAVIEW_LIB_CATALYST_DIR");
    if (!paraview_lib_catalyst_dir) {
        std::cout << "Please set PARAVIEW_LIB_CATALYST_DIR (see install instructions)" << std::endl;
        return EXIT_FAILURE;
    }

    // Get option
    std::string option;
    if (argc == 3)
        option = std::string(argv[2]);

    // Create catalyst adaptor
    std::cout << "Simple:" << std::endl
              << "=======" << std::endl;
    CatalystAdaptor catalyst(paraview_lib_catalyst_dir, pipeline_script, option);

    // Init some points
    std::vector<Point> points;
    points.emplace_back(0, 0, 0, 0.1, 2.2, 3.5);
    points.emplace_back(0.5, -4, 0.9, -0.3, 0.2, 0);
    points.emplace_back(2.5, 2.5, -5, 2.5, -2, 0);
    points.emplace_back(2.5, 0, 0, 1, 1, 1);

    // Perform simulation
    int N_iter = 20;
    double dt = 0.1;
    for (int iter = 0; iter < N_iter; iter++) {
        // Render
        catalyst.execute(iter, dt*iter, points);

        // Update points
        for (Point &point : points) {
            point.UpdatePosition(dt);
        }
    }

    return EXIT_SUCCESS;
}
