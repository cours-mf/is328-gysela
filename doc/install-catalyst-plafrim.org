* Utilisation sur Plafrim
Nous allons utiliser un système bicéphale :
- Une installation Paraview+Catalyst sans client graphique sur Plafrim (car mauvaise
  qualité ~ssh -X~ + drivers vidéo pas à jour). C'est avec celle-ci que vous ferez vos
  rendus
- Une installation locale avec client graphique pour préparer vos rendus et générer les
  scripts Catalyst

** Bibliothèque Paraview + Catalyst sur Plafrim
Vous avez à votre disposition une installation ParaView Catalyst sans client graphique
dans ~/home/bouzat/visu_opt/~. Pour l'utiliser, ajouter le code suivant à votre .bashrc :
#+begin_src bash
  # Use headless ParaView+Catalyst installation
  source /home/bouzat/visu_opt/source_visu_opt.sh
#+end_src

** Client graphique en local sur votre machine
Afin d'avoir la même version que sur Plafrim (pour la compatibilité des scripts Catalyst),
merci de télécharger la version *5.11.2* en version binaire pour votre système depuis
[[https://www.paraview.org/download/]].


** Montage sshfs (ou autres suivant système)
Pour pouvoir éditer le code en local et visualiser directement les images rendus sans
avoir à les ~scp~ à chaque fois, faites un montage ~sshfs~. En supposant que vous avez
cloné le dépôt dans ~$HOME/is328-gysela~ sur plafrim, lancer sur votre machine :
#+begin_src bash
  mkdir -p $HOME/is328-gysela/
  sshfs -f -d -o reconnect -o ServerAliveInterval=3 plafrim:is328-gysela/ $HOME/is328-gysela/
#+end_src
