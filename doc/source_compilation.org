#+OPTIONS: tex:t
#+OPTIONS: toc:t

* Compilation depuis les sources
** Rappel : chaîne de compilation
Afin d'installer un logiciel depuis les sources, on passe par plusieurs étapes :
- *Configuration* : Détection de l'architecture (disponibilité et taille des différents
  type de données : ~short~, ~int~, ~float~, ~double~..., biliothèques standards...),
  recherche des dépendances, adaptation au compilateur choisi...
- *Génération* : Création des fichiers contenant les règles de compilation
- *Compilation* : Traduction des sources en binaires, exécutables ou
  linkables (vous avez déjà eu un cours là-dessu)
- *Installation* : Déplacement des fichiers générés à l'endroit désiré
  (~/usr/{bin,lib,include,share,etc...} par défaut)

** Outils
*** Low-end build system
Pour la partie *Compilation* et *Installation* :
- ~make~
- ~ninja~
  
*** High-end build system
Pour la partie *Configuration* et *Génération* :
- ~autotools~
- ~cmake~
- ~meson~
  
** Différentes chaînes
*** Autotools
- ~autoconf~ :: génère un script ~configure~ depuis ~configure.am~
- ~automake~ :: génère des ~Makefile.in~ depuis des ~Makefile.am~
- ~autoreconf -fi~ :: fait les deux
- ~configure~ :: configure et génère
- ~make~ :: compile
- ~make install~ :: installe

#+begin_src bash
  # Sources
  git clone ....

  # Re-génère
  autoreconf -fi
  
  # Configuration
  ./configure

  # Compilation
  make -j 4

  # installation
  make install
#+end_src

*** CMake
- ~cmake~ :: configure et génère
- ~cmake --build~ :: compile via ~make~ ou ~ninja~
- ~cmake --install~ :: installe

#+begin_src bash
  # Sources
  git clone ....

  # Configuration
  cmake -B build/ -G<"Unix Makefiles"/Ninja> -DCMAKE_INSTALL_PREFIX=<path> ...

  # Compilation
  cmake --build build/ -j 4

  # installation
  cmake --install build/
#+end_src

*** Meson
- ~meson setup~ :: configure et génère
- ~meson compile~ :: compile via ~make~ ou ~ninja~
- ~meson install~ :: installe
#+begin_src bash
  # Sources
  git clone ....

  # Configuration
  meson setup build/

  # Compilation
  meson compile -C build/

  # installation
  meson install -C build/
#+end_src
