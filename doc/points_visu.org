* Exemple points avec visualisation
Restons sur l'exemple précédent, nous allons manipuler Paraview pour générer un nouveau
script Python ~catalyst_pipelin_with_rendering.py~ qui fera cette fois un rendu.

** Générer le script
L'idée est simplement d'utiliser des données similaires à celles générée par notre
similation et de créer un pipeline de rendu pour le sauvegarder sous forme de script. Le
processus est le suivant :
- Créer un sous-ensemble de sources représentatif de notre jeu de données (Menu : ~Sources~)
- Appliquer les filtres et transformations souhaités (Menu : ~Filters~)
- Positionner la caméra
- Ajouter un extracteur pour le rendu (Menu : ~Extractors > Image~)
- Sauvegarder le script Catalyst ~catalyst_pipeline_with_rendering.py~ (Menu : ~File > Save Catalyst State~)

- Note :: Lors de la sauvegarde, il est possible de choisir quand se déclenche le rendu

*** Questions et remarques
- Quelle source pour émuler notre simulation ?
- Pas de filtre pour l'instant (affichez simplement les axes)
- Changez la représentation des points pour qu'ils soient plus visibles (~Properties >
  Display > Representation > Points~ + ~Render Points As Spheres~)
- Comment positionner la caméra ?
- N'oubliez pas l'extracteur (PNG ou JPG)
- Executez le script Python sauvegardé directement avec Python (celui de Paraview)
  ~pvpython catalyst_pipeline_with_rendering.py~. Que se passe-t-il ?
- !!!!!!! Prenez une capture d'écran de votre fenêtre Paraview avant d'exporter le
  script. La RenderView, le pipeline et les propriétés de la source doivent être visible
  
** Rendre notre simulation
Quelques modifications sur notre script ~catalyst_pipeline_with_rendering.py~ sont
nécessaires pour le brancher sur Catalyst :
- Changer la source pour que cela devienne notre simulation :
  - Par exemple, la ligne suivante :
  #+begin_src python
    source1 = Source(registrationName='Source1')
  #+end_src
  - Devient (ne pas oublier de changer la variable dans le reste du code) :
  #+begin_src python
    producer = TrivialProducer(registrationName='my_points')
  #+end_src
- Supprimer la partie ~__main__~ et récupérer la fonction ~catalyst_execute()~ du
  ~catalyst_pipeline.py~.


*** Questions
- Est-ce que ça marche ? Est-ce que les points bougent bien ? Woot
- N'oubliez pas une petite note et une image dans le rapport.
