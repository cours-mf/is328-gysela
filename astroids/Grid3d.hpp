#ifndef GRID3D_H
#define GRID3D_H

#include <vector>

class Grid3d {
public:
	std::vector<int> leading_dims;
	std::vector<double> mini;
	std::vector<double> maxi;
	std::vector<double> spacing;
    int N_nodes;
    Grid3d(std::vector<int> ld, std::vector<double> min, std::vector<double> max) {
        leading_dims = ld;
        mini = min;
        maxi = max;
        N_nodes = ld[0]*ld[1]*ld[2];
        spacing.emplace_back((maxi[0]-mini[0])/(ld[0]-1));
        spacing.emplace_back((maxi[1]-mini[1])/(ld[1]-1));
        spacing.emplace_back((maxi[2]-mini[2])/(ld[2]-1));
    };
};
    
#endif //GRID3D_H
