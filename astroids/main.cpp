#include "Astroid.hpp"
#include "CatalystAdaptor.hpp"
#include "Galaxy.hpp"
#include "Grid3d.hpp"

int
main(int argc, char *argv[])
{
	std::srand(1234);
	if (argc < 2) {
		std::cerr << "Usage: ./astroids <path_to_pipeline_script>"
				  << std::endl;
		return EXIT_FAILURE;
	}

	// Get pipeline python script
	const char* pipeline_script = argv[1];

	// Get libcatalyst-paraview.so directory
	const char* paraview_lib_catalyst_dir = std::getenv("PARAVIEW_LIB_CATALYST_DIR");
	if (!paraview_lib_catalyst_dir) {
		std::cout << "Please set PARAVIEW_LIB_CATALYST_DIR (see install instructions)" << std::endl;
		return EXIT_FAILURE;
	}

	// Create catalyst adaptor
	CatalystAdaptor catalyst(paraview_lib_catalyst_dir, pipeline_script);

	// Create simulation
	Astroid asteroid(10e20, 10e5);
	Astroid terrestrial_planet(10e24, 10e6);
	Astroid gas_planet(10e28, 10e7);
	Galaxy galaxy(1e10, 1e12, 10e30);
	galaxy.add_astroids(gas_planet, 4);
	galaxy.add_astroids(terrestrial_planet, 6);
	galaxy.add_astroids(asteroid, 10);

	// Setup a grid for diagnostics
	int grid_ld = 15; // number of mesh node (= number of cells+1)
	double base = 1e2;
	double factor = 1e10;
	std::vector<double> mini{-base, -base, -base};
	std::vector<double> maxi{base, base, base};
	std::vector<int> dims{grid_ld, grid_ld, grid_ld};
	Grid3d grid(dims, mini, maxi);

	// Render initial visualisations
	std::vector<double> field = galaxy.diagnostics(grid, factor);
	catalyst.execute(0, 0, galaxy.astroids, grid, field);

	// Run simulation
	int N_iter = 30;
	double dt = 2e5;
	for (int iter = 0; iter < N_iter; iter++) {
		// Compute gravitationnal forces and astroids acceleration
		galaxy.compute_astroids_acceleration();
		//galaxy.output_astroids();

		// Move astroids and update velocities
		galaxy.update_astroids_velocity_and_position(dt);

		// Compute galaxy characteristics
		std::vector<double> gravity_field = galaxy.diagnostics(grid, factor);

		// Render visualisations
		catalyst.execute(iter+1, (iter+1)*dt, galaxy.astroids, grid, gravity_field);
	}

	return EXIT_SUCCESS;
}
