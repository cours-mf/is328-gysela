#ifndef GALAXY_HPP
#define GALAXY_HPP

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <vector>

#include "Astroid.hpp"
#include "Grid3d.hpp"

#define PI 3.14159265358979323846
#define G 6.674*10e-11

class Galaxy {
public:
	double Re; // External torus radius
	double Ri; // Internal torus radius;
	double R; // Poloidal revolution axis radius
	double a; // Poloidal radius;

	std::vector<Astroid> astroids;

	Galaxy(double inner_radius, double outer_radius, double black_hole_mass):
		Ri(inner_radius),Re(outer_radius),R((Re + Ri)/2),a((Re - Ri)/2) {
		// Add central back hole
		astroids.emplace_back(black_hole_mass, inner_radius);
	};

	void add_astroids(Astroid &original_astroid, int N) {
		for (int i = 0; i < N; i++) {
			astroids.emplace_back(original_astroid);
			Astroid &ast = astroids.back();

			// Draw a random position on torus
			double r = a*((double)std::rand()/RAND_MAX);
			double theta = 2*PI*((double)std::rand()/RAND_MAX);
			double phi = 2*PI*((double)std::rand()/RAND_MAX);
			ast.set_pos((R + r*std::cos(theta))*cos(phi),
						(R + r*std::cos(theta))*sin(phi),
						r*std::sin(theta));

			// Set astroid on orbit around black hole with horizontal velocity,
			// orthogonal to ur
			Astroid &black_hole = astroids[0];
			double v_norm = std::sqrt(G*black_hole.mass/ast.distance_to(black_hole));
			ast.set_vel(-std::sin(phi)*v_norm,
						std::cos(phi)*v_norm,
						0);
		}
	};

	void compute_astroids_acceleration(void) {
		for (Astroid &ast : astroids) {
			if (&ast == &astroids.front())
				continue;

			ast.ax = 0;
			ast.ay = 0;
			ast.az = 0;
			for (Astroid &ast1 : astroids) {
				if (&ast == &ast1)
					continue;

				// Compute vector between astroids
				double x = ast1.x - ast.x;
				double y = ast1.y - ast.y;
				double z = ast1.z - ast.z;

				// Normalize for unitary vector
				double norm = std::sqrt(x*x + y*y + z*z);
				x /= norm;
				y /= norm;
				z /= norm;

				// Compute force
				double F_norm = ast1.mass/(norm*norm);
				ast.ax += F_norm*x;
				ast.ay += F_norm*y;
				ast.az += F_norm*z;
			}

			// Update acceleration
			ast.ax *= G;
			ast.ay *= G;
			ast.az *= G;
		}
	};

	void update_astroids_velocity_and_position(double dt) {
		for (Astroid &ast : astroids) {
			if (&ast == &astroids.front())
				continue;

			// Update velocity
			ast.vx += dt*ast.ax;
			ast.vy += dt*ast.ay;
			ast.vz += dt*ast.az;

			// Update position
			ast.x += dt*ast.vx;
			ast.y += dt*ast.vy;
			ast.z += dt*ast.vz;
		}
	};

	double compute_gravity_field(double x, double y, double z) {
		double F_x = 0, F_y = 0, F_z = 0;
		for (Astroid &ast : astroids) {
			// Compute vector between astroids
			double vec_x = ast.x - x;
			double vec_y = ast.y - y;
			double vec_z = ast.z - z;

			// Normalize for unitary vector
			double norm = std::sqrt(vec_x*vec_x + vec_y*vec_y + vec_z*vec_z);
			vec_x /= norm;
			vec_y /= norm;
			vec_z /= norm;

			// Compute force
			double F_norm = ast.mass/(norm*norm);
			F_x += F_norm*vec_x;
			F_y += F_norm*vec_y;
			F_z += F_norm*vec_z;
		}

		return G*std::sqrt(F_x*F_x + F_y*F_y + F_z*F_z);
	};
	
	std::vector<double> diagnostics(Grid3d &grid, double factor) {
		std::vector<double> gravity_field(grid.N_nodes, 0);
		for (int ix = 0; ix < grid.leading_dims[0]; ix++) {
			for (int iy = 0; iy < grid.leading_dims[1]; iy++) {
				for (int iz = 0; iz < grid.leading_dims[2]; iz++) {
					int id = ix + iy*grid.leading_dims[0] +
						iz*grid.leading_dims[0]*grid.leading_dims[1];
					double x = grid.mini[0] + ix*grid.spacing[0];
					double y = grid.mini[1] + iy*grid.spacing[1];
					double z = grid.mini[2] + iz*grid.spacing[2];

					gravity_field[id] = compute_gravity_field(x*factor,
															  y*factor,
															  z*factor);
				}
			}
		}

		return gravity_field;
	};

	void output_astroids() {
		fprintf(stdout, "Astroids:\n");
		for (Astroid &ast : astroids) {
			ast.debug();
		}
		fflush(stdout);
	}
};

#endif // GALAXY_HPP
