#ifndef ASTROID_HPP
#define ASTROID_HPP

#include <cmath>
#include <iostream>

class Astroid {
public:
	double mass;
	double radius;
	double x;
	double y;
	double z;
	double vx;
	double vy;
	double vz;
	double ax;
	double ay;
	double az;

	Astroid(const double mass, const double radius):mass(mass),radius(radius) {
		this->set_pos(0, 0, 0);
		this->set_vel(0, 0, 0);
		this->set_acc(0, 0, 0);
	};
	Astroid(const Astroid &a):mass(a.mass),radius(a.radius),x(a.x),y(a.y),z(a.z),
							  vx(a.vx),vy(a.vy),vz(a.vz),ax(a.ax),ay(a.ay),az(a.az) {};

	void set_pos(const double x, const double y, const double z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}

	void set_vel(const double vx, const double vy, const double vz) {
		this->vx = vx;
		this->vy = vy;
		this->vz = vz;
	}

	void set_acc(const double ax, const double ay, const double az) {
		this->ax = ax;
		this->ay = ay;
		this->az = az;
	}

	double distance_to(const Astroid &a) {
		return std::sqrt((x-a.x)*(x-a.x) + (y-a.y)*(y-a.y) + (z-a.z)*(z-a.z));
	}

	void debug() {
		fprintf(stdout, "    (%10.3e,%10.3e,%10.3e,\n     %10.3e,%10.3e,%10.3e\n     %10.3e,%10.3e,%10.3e\n    %10.3e,%10.3e)\n", x, y, z, vx, vy, vz, ax, ay, az, mass, radius);
	}
};

#endif // ASTROID_HPP
